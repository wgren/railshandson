class Doctor < ActiveRecord::Base

  #Built in validator
  validates :last_name, :age, presence: true

  #Custom validation
  validate :doctor_minimum_age

  belongs_to :hospital

  #Helper method
  def display_name
  end

  #To fail validation
  #errors.add(:name_of_attribute, "Customer error string")
  def doctor_minimum_age
  end

end
