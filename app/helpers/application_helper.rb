module ApplicationHelper

  def navbar_list_item(displayname, path, htmlopts={})
    selected = current_page?(path)
    link = link_to displayname, path, htmlopts
    "<li class=\"#{'active' if selected} \"> #{link} </li>".html_safe
  end


end
