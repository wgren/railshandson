require 'rails_helper'

RSpec.describe DoctorsController, :type => :controller do

  let(:doctor) {FactoryGirl.create(:doctor)}

  SESSION_PARAMS= {foo: "bar"}

  describe "GET edit" do

    it "redirects if no password" do
      get :edit, {:id => doctor.to_param}, {}
      expect(response).to redirect_to(doctors_path)
    end

    it "allows if password" do
      get :edit, {:id => doctor.to_param}.merge(SESSION_PARAMS)
      expect(response).to render_template(:edit)
    end

  end

end
