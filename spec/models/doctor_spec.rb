require "rails_helper"

RSpec.describe Doctor, :type => :model do

  FG = FactoryGirl

  let(:doctor) { FG.create(:doctor) }

  context "age validation" do

    it "has valid factory" do
      expect(doctor.valid?).to be true
    end

    it "works with nil" do
      expect(FG.build(:doctor, age: nil).valid?).to be false
    end

    it "refuses low age" do
      expect(FG.build(:doctor, age: 17).valid?).to be false
    end

    it "refuses really low age" do
      expect(FG.build(:doctor, age: -2000).valid?).to be false
    end

    it "accepts 18" do
      expect(FG.build(:doctor, age: 18).valid?).to be true
    end

    it "accepts high" do
      expect(FG.build(:doctor, age: 75).valid?).to be true
    end

    it "refuses too high" do
      expect(FG.build(:doctor, age: 90).valid?).to be false
    end

  end

  context "display name" do

    it "has method" do
      expect(doctor).to respond_to(:display_name)
    end

    it "works with nils" do
      expect(FG.build(:doctor, first_name: nil, last_name: nil).display_name).to_not be_nil
    end

    it "concatenates first and last names" do
      expect(FG.build(:doctor, first_name: "Hans", last_name: "Rosling").display_name).to eq("Hans Rosling")
    end

    it "trims if only lastname" do
      expect(FG.build(:doctor, first_name: nil, last_name: "Galenos").display_name).to eq("Galenos")
    end

  end



end