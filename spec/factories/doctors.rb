FactoryGirl.define do
  factory :doctor do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    age {rand(18..80)}
    association :hospital, factory: :hospital
    end
end