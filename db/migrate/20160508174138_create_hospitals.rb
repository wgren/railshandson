class CreateHospitals < ActiveRecord::Migration
  def change
    create_table :hospitals do |t|
      t.string :name, null: false
      t.timestamps null: false
    end

    add_reference :doctors, :hospital, index: true, foreign_key: true

  end
end
