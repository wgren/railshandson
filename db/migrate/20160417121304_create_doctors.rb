class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :first_name
      t.string :last_name, null: false
      t.string :email
      t.text :quote
      t.integer :age, null: false

      t.timestamps null: false
    end
  end
end
