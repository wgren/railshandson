# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Doctor.create(last_name: "Hippokrates", age: 80, quote: "I will, according to my ability and judgment, prescribe a regimen for the health of the sick; but I will utterly reject harm and mischief")
Doctor.create(first_name: "Florence", last_name: "Nightingale", age: 33, quote: "The very first requirement in a hospital is that it should do the sick no harm.")

