# Rails hands on lab

## Installation/Förberedelse ##

Dessa steg gäller för Windows, Linux och MacOS.

1. Ladda hem och installera senaste versionen av virtualbox ([https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads))

2. Ladda hem och installera senaste versionen av vagrant ([https://www.vagrantup.com/downloads.html](https://www.vagrantup.com/downloads.html))

3. Klona ner detta repository.

4. Ståendes i rotkatalogen av repositoriet kör `vagrant up`. Kommandot kan ibland ta ett tag då OS "imagen" ska laddas ner och nätverket m.m skall konfigureras.

5. Kör `vagrant ssh` för att logga in labbmiljön (Windows-användare behöver eventuellt installera en ssh klient separat. Tips: [tortoisegit](https://tortoisegit.org/) bundlar en klient i sin installation).

7. Kör `cd railshandson && bundle && bin/rails s` och öppna sedan `http://127.0.0.1:3000/` i webbläsaren. Om allt gått bra skall labbens instruktioner visas.

8. Tryck `ctrl - c` för att avsluta utvecklingsservern.

9. Kör `exit` för att avsluta SSH-sessionen

9. Kör `vagrant halt` för att avsluta utvecklingsmiljön